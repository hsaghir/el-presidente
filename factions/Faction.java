package factions;
public class Faction {
	
	private FactionType name;
	private int partisantsNumber;
	private int satisfaction;
	
	public Faction(FactionType name) {
		this.partisantsNumber = 15;
		this.satisfaction = 50;
		this.name = name;
	}
	
	public FactionType whichFaction() {
		return this.name;
	}
	
	public int getSatisfaction() {
		return satisfaction;
	}
	
	public int getPartisantsNumber() {
		return partisantsNumber;
	}
	
	public void factionInformations() {
		System.out.println("* Name : " + name);
		System.out.println("* Partisant number : " + partisantsNumber);
		System.out.println("* Satisfaction : " + satisfaction);
	}
}
