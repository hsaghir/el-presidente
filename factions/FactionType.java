package factions;

public enum FactionType {
	 Communists,
	 Capitalists,
	 Liberals,
	 Religious,
	 Militarists,
	 Ecologists,
	 Nationalists,
	 Loyalists
}
