package principal;

import events.Season;

public class MainApp {
	public static int difficulty;
	public static int season;

	public static void main(String[] args) {
		Island theIsland = new Island();
		season = 0;

		boolean life = true;

		System.out.println("--- El Presidente ---");
		while (life) {
			theIsland.islandInformations();
			//theIsland.islandFactionsInformations();
			System.out.println("##### " + Season.valueToSeason(season) + " #####");
			season = (season + 1) % 4;
			
			if ((difficulty == 0 && theIsland.getGeneralSatisfaction() < 10)
					|| (difficulty == 1 && theIsland.getGeneralSatisfaction() < 30)
					|| (difficulty == 2 && theIsland.getGeneralSatisfaction() < 50)) {
				life = false;
				System.out.println("Coup d'état ! Partie terminée.");
			}
		}
	}

}
