package principal;

import factions.Faction;
import factions.FactionType;

public class Island {
	private int industry; // %
	private int agriculture; // %
	private int Treasury; // $
	private int food;
	private int generalSatisfaction; // %
	private int populationNumber;

	private Faction[] population;

	public Island() {
		industry = 15;
		agriculture = 15;
		Treasury = 200;
		population = new Faction[] { new Faction(FactionType.Communists), new Faction(FactionType.Capitalists),
				new Faction(FactionType.Liberals), new Faction(FactionType.Religious),
				new Faction(FactionType.Militarists), new Faction(FactionType.Ecologists),
				new Faction(FactionType.Nationalists), new Faction(FactionType.Loyalists) };
		updateSatisfaction();
	}

	private void updateSatisfaction() {
		updatePopulationNumber();
		generalSatisfaction = 0;
		for (Faction faction : population) {
			generalSatisfaction += (faction.getSatisfaction() * faction.getPartisantsNumber());
		}
		generalSatisfaction /= populationNumber;
	}
	
	private void updatePopulationNumber() {
		populationNumber = 0;
		for (Faction faction : population) {
			populationNumber += faction.getPartisantsNumber();
		}
	}

	public void oneYearMoney() {
		Treasury += industry * 10;
		food += agriculture * 40;
	}

	public void islandInformations() {
		System.out.println("* Industry : " + industry + " %");
		System.out.println("* Agricuture : " + agriculture + " %");
		System.out.println("* Treasury : " + Treasury+ " $");
		System.out.println("* Food : " + food);
		System.out.println("* General satisfatction : " + generalSatisfaction + " %");
	}
	
	public void islandFactionsInformations() {
		for (Faction faction : population) {
			System.out.println();
			faction.factionInformations();
		}
	}
	
	public int getGeneralSatisfaction() {
		return generalSatisfaction;
	}

}
