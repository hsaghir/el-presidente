package events;
import factions.FactionType;

public class Action {
	private FactionType faction;
	private int satisfactionGain;
	
	public Action(FactionType faction, int satisfactionGain) {
		this.faction = faction;
		this.satisfactionGain = satisfactionGain;
	}
}
