package events;

public class Event {
	private String message;
	private Choice[] choices;
	private Season[] seasons;
	private String goodChoice;
	
	
	public Event(String message, Choice[] choices, Season[] seasons, String choice) {
		this.message = message;
		this.choices = choices;
		this.seasons = seasons;
		this.goodChoice = choice;
	}
}
