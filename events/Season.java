package events;

public enum Season {
	WINTER,
	SPRING,
	SUMMER,
	AUTUMN;
	
	public static Season valueToSeason(int value) {
		switch(value) {
			case 0 : return WINTER;
			case 1 : return SPRING;
			case 2 : return SUMMER;
			case 3 : return AUTUMN;
		}
		return null;
	}
}
