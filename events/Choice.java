package events;
import java.util.ArrayList;

public class Choice{
	private String Title;
	private ArrayList<Action> actions;
	
	public Choice(String title, ArrayList<Action> actions) {
		super();
		Title = title;
		this.actions = actions;
	}

}
